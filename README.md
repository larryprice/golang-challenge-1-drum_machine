See [http://golang-challenge.com/go-challenge1/](http://golang-challenge.com/go-challenge1/) for details.

Context: Your boss built a drum machine many years ago and the input files were built to look like this:

``` pattern_1.splice
Saved with HW Version: 0.808-alpha
Tempo: 120
(0) kick     |x---|x---|x---|x---|
(1) snare    |----|x---|----|x---|
(2) clap     |----|x-x-|----|----|
(3) hh-open  |--x-|--x-|x-x-|--x-|
(4) hh-close |x---|x---|----|x--x|
(5) cowbell  |----|----|--x-|----|
```

The file consists of the following data:

* Header
  * HW version
  * Tempo
* Body
  * Track number (in parentheses)
  * Instrument type (kick, snare, clap, etc.)
  * Musical notes (every "x" means play a sound)

Unfortunately, your boss has lost the drum machine code. All he has left is a set of binary-encoded files that he doesn't know how to read. You're being taken off project work for the week in order to reconstruct the original files from the binary-encoded files.

We start the Go challenge with a directory of binary-encoded files called "fixtures." We have a short file called `decoder.go` which is described as the entry point to the reader. We have a test file for decoder called `decoder_test.go` which contains a test which iterates through our fixtures and verifies our Reader returns the right data. Any questions about the problem before I move on to implementation?

Decoder
|- PatternReader
||- PatternParser
|||- PatternHeaderParser
||||- PatternHeader
|||- PatternTracksParser
||||- Pattern
